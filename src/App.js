import React from 'react';
import { Row, Col } from 'antd';
import 'antd/dist/antd.css';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Menu from './components/menu/Menu';
import Products from './components/products/Producs';
import Inventary from './components/inventary/Inventary';

class App extends React.Component{

  render(){

    return(
      <>
        <Router>
          <Row>
            <Col span={4}>
              <Menu />
            </Col>

            <Col span={20}>
              <div style={{ width:'90%', margin: 'auto' }}>
              <Switch>
                <Route path='/' exact component={Products} />
                <Route path='/inventary' exact component={Inventary} />
              </Switch>
              </div>
            </Col>
          </Row>
        </Router>
      </>
    );

  }

  
}

export default App;
