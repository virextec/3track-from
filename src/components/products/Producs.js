import React, { useState } from 'react';
import { Table, Radio, Divider, Button, Tooltip, Modal  } from 'antd';
import { getData } from '../../services/WebService';
import { CheckCircleTwoTone, MinusCircleTwoTone, CloseCircleTwoTone } from '@ant-design/icons';

const columns = [
  {
    title: 'Estado',
    dataIndex: 'estado',
    key: 'estado'
  },
  {
    title: 'Imagen',
    dataIndex: 'imagen',
    key: 'imagen'
  }, 
  {
    title: 'Nombre',
    dataIndex: 'nombre',
    key: 'nombre',
    with: '12'
  },
  {
    title: 'FechaC',
    dataIndex: 'fechaC',
    key: 'fechaC',
  },
  {
    title: 'Disponible',
    dataIndex: 'disponible',
    key: 'disponible',
  },
  {
    title: 'Precio',
    dataIndex: 'precio',
    key: 'precio'
  },
  {
    title: 'Accion',
    dataIndex: 'accion',
    key: 'x',

    render: () => {
      return(
        <>
        <Tooltip title="Activar">
          <Button shape="circle" icon={<CheckCircleTwoTone twoToneColor="#52c41a" style={{fontSize: '20px'}}/>} />          
        </Tooltip>

        <Tooltip title="Inactivar ">
          <Button shape="circle" icon={<MinusCircleTwoTone style={{fontSize: '20px'}}/>} />          
        </Tooltip>

        <Tooltip title="Eliminar">
          <Button shape="circle" icon={<CloseCircleTwoTone twoToneColor="#eb2f96" style={{fontSize: '20px'}}/>} />          
        </Tooltip>
           
        </>
      )
    }
  }
];


const data = [
  {
    id: 1,
    estado: 'Activo',
    nombre: 'Mike',
    fechaC: '20/20/20',
    disponible: 20,
    precio: 'S/. 400.00',
  },
  {
    id: 2,
    estado: 'Inactivo',
    nombre: 'Mike',
    fechaC: '20/20/20',
    disponible: 20,
    precio: 'S/. 400.00',
  },
  {
    id: 3,
    estado: 'Activo',
    nombre: 'Mike',
    fechaC: '20/20/20',
    disponible: 20,
    precio: 'S/. 400.00',
  },
  {
    id: 4,
    estado: 'Activo',
    nombre: 'Mike',
    fechaC: '20/20/20',
    disponible: 20,
    precio: 'S/. 400.00',
  },
  {
    id: 5,
    estado: 'Activo',
    nombre: 'Mike',
    fechaC: '20/20/20',
    disponible: 20,
    precio: 'S/. 400.00',
  },
  {
    id: 6,
    estado: 'Activo',
    nombre: 'Mike',
    fechaC: '20/20/20',
    disponible: 20,
    precio: 'S/. 400.00',
  },
  
];

class Products extends React.Component{
  constructor(){
    super();

    this.state={
      setIsModalVisible: false,
      accountId: String,
      credits: Number,
      maxCredits: Number 
    }

  }

  componentDidMount =  () => {
    this.miData();
  }

  miData = async () => {

    return await getData().then(res =>{      
      console.log('la data de respuesta es', res);

      if(res.data.accountId==null){
        this.setState({
          accountId: 'accountId  es null'
        })
      }else{
        this.setState({
          accountId: res.data.accountId
        })
      }
    })
  }

  setIsModalVisible(x){
    this.isModalVisible = x
  }

    render(){ 
      const {
        isModalVisible, setIsModalVisible, accountId
      } = this.state;

      
      
      const showModal = () => {
        this.setIsModalVisible(true);
      };
      
      const handleOk = () => {
        this.setIsModalVisible(false);
      };
      
      const handleCancel = () => {
        this.setIsModalVisible(false);
      }; 

        return(
          <>
          <div>
            <h2>Administracion de Inventario</h2>  
            <div>
              <Radio.Group>
                <h3>Filtros Abel: </h3>
                <Radio>Todos Abel</Radio>
                <Radio value="A">Activos</Radio>
                <Radio value="I">Inactivos</Radio>
              </Radio.Group>
              <div>
                <p><b>accountId: </b> {accountId}</p>
              </div>
              <p align={'right'}>
                <Button type="primary" onClick={showModal}>Nuevo Productos</Button>
              </p>
              
              <Divider /> 
              <Table key={data.id} dataSource={data} columns={columns} />
            </div>
          </div>

          <Modal title="Basic Modal" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
            <p>Some contents...</p>
            <p>Some contents...</p>
            <p>Some contents...</p>
          </Modal>
          </>
        )
    }
}

export default Products;